import "./listnotes.css";
import { Link } from "react-router-dom";
import ReactMarkdown from "react-markdown";

function ListNotes({ note }) {
  // const [showNoteInfo, setShowNoteInfo] = useState(false);

  // async function removeNote(idNota) {
  //   try {
  //     await axios.delete(`http://localhost:4000/grupos/notas/${idNota}`, {
  //       headers: {
  //         Authorization: token,
  //       },
  //     });
  //     removeNoteToUser(idNota);
  //   } catch (err) {
  //     setError(`Error borrando nota: ${err.response.data.message}`);
  //     console.log("Error", err.response);
  //   }
  // }

  // function onClick() {
  //   let bool = true;

  //   if (showNoteInfo === bool) {
  //     bool = !bool;
  //   }
  //   setShowNoteInfo(bool);
  // }

  return (
    <li /*onClick={onClick}*/ className="note-item-container">
      <Link to={`/userhome/notes/${note.id}`} className="note-box">
        <h1>{note.titulo}</h1>
        <ReactMarkdown>{note.contenido}</ReactMarkdown>
      </Link>

      {/* {showNoteInfo && (
        <div>
          <ReactMarkdown>{note.contenido}</ReactMarkdown>
          <section>{new Date(note.fecha_creacion).toLocaleString()}</section>
          <button
            onClick={() => removeNote(note.id)}
            className="remove-note-button"
          >
            <BsTrash />
          </button>
        </div>
      )} */}
    </li>
  );
}

export default ListNotes;
