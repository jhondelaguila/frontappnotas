import { useState, useContext } from "react";
import axios from "axios";
import { ErrorContext } from "../../components/context/ErrorContext";
import { useHistory } from "react-router";

function IntroduceRecoveryCode() {
  const [codigoRecuperacion, setCodigoRecuperacion] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [repeatedNewPassword, setRepeatedNewPassword] = useState("");
  const { error, setError } = useContext(ErrorContext);
  let history = useHistory();

  async function onSubmitNewPassword(event) {
    event.preventDefault();

    const error = validateRegisterForm(
      codigoRecuperacion,
      newPassword,
      repeatedNewPassword
    );

    if (error) {
      setError(error);
      return;
    }

    try {
      await axios.put(`http://localhost:4000/Usuarios/contrasena/reset`, {
        codigoRecuperacion,
        newPassword,
      });

      history.push("/login");
    } catch (err) {
      setError(`Error cambiando contraseña: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return (
    <div>
      Nueva contraseña
      <div className="newpassword-form">
        <div className="newpassword-container">
          <h1>Cambiar contraseña</h1>
          <form onSubmit={onSubmitNewPassword}>
            <label className="field-container">
              Código de Recuperación*
              <input
                className="field"
                value={codigoRecuperacion}
                onChange={(event) => setCodigoRecuperacion(event.target.value)}
                type="text"
              />
            </label>
            <label className="field-container">
              New Password*
              <input
                // required
                className="field"
                value={newPassword}
                onChange={(event) => setNewPassword(event.target.value)}
                type="password"
              />
            </label>
            <label className="field-container">
              Repeat New Password*
              <input
                // required
                className="field"
                value={repeatedNewPassword}
                onChange={(event) => setRepeatedNewPassword(event.target.value)}
                type="password"
              />
            </label>
            <div className="newpassword-button-container">
              <input
                className="newpassword-button"
                type="submit"
                value="Cambiar contraseña"
              />
            </div>
            {error && <div className="error-label">{error} </div>}
          </form>
        </div>
      </div>
    </div>
  );
}

export default IntroduceRecoveryCode;

function validateRegisterForm(
  codigoRecuperacion,
  newPassword,
  repeatedNewPassword
) {
  if (!codigoRecuperacion) {
    return "El campo password es obligatorio";
  }

  if (!newPassword) {
    return "El campo password es obligatorio";
  }

  if (!repeatedNewPassword) {
    return "El campo password es obligatorio";
  }

  const isValidPassword = newPassword === repeatedNewPassword;

  if (!isValidPassword) {
    return "Las contraseñas deben coincidir";
  }
}
