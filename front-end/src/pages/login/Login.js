import { useContext, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import "./login.css";
import logo from "../../static/Wmarl_Logo.png";
import { UserContext } from "../../components/context/UserContext";
import { ErrorContext } from "../../components/context/ErrorContext";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { setError } = useContext(ErrorContext);
  const { login } = useContext(UserContext);

  let history = useHistory();

  async function onSubmitLogin(event) {
    event.preventDefault();

    console.log("Email: ", email);
    console.log("password: ", password);

    try {
      const response = await axios.post(
        "http://localhost:4000/Usuarios/login",
        {
          email,
          password,
        }
      );
      const data = response.data.data;

      login(data);

      history.push("/userhome");
    } catch (err) {
      setError(`Error logeando: ${err.response.data.message}`);
      console.log("Error", err.response);
    }

    // try {

    // } catch (err) {
    //   setError(`Error obteniendo grupos en: ${err.response.data.message}`);
    //     console.log("Error", err.response);
    // }
  }

  return (
    <div className="login-container">
      <img className="wmark-login" src={logo} alt="logo login" />

      <h1>Log in</h1>
      <form className="login-form" onSubmit={onSubmitLogin}>
        <label className="field-container">
          Email
          <input
            className="field-login"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </label>
        <label className="field-container">
          Password
          <input
            className="field-login"
            type="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </label>
        <div className="login-button-container">
          <input className="login-button" type="submit" value="Log in" />
        </div>

        <div>
          <a href="/userhome/sendrecoverycode"> ¿Olvidaste la contraseña?</a>
        </div>
        <div>
          <p>
            ¿No tienes cuenta?<a href="/register"> Registrate</a>
          </p>
        </div>
      </form>
    </div>
  );
}

export default Login;
