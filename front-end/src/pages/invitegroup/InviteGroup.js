import { useState, useContext } from "react";
import { useParams } from "react-router";
import axios from "axios";
import { ErrorContext } from "../../components/context/ErrorContext";
import { UserContext } from "../../components/context/UserContext";

function InviteGroup() {
  const { setError } = useContext(ErrorContext);
  const { user } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(null);

  const token = user.token;
  const { idGrupo } = useParams();

  async function sendEmailInvitation(event) {
    event.preventDefault();
    try {
      const response = await axios.put(
        `http://localhost:4000/grupos/invitar-grupo/${idGrupo}`,
        { email },
        { headers: { Authorization: token } }
      );

      const emailMessage = response.data.message;

      setMessage(emailMessage);
    } catch (err) {
      setError(`Error enviando invitación: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return message === null ? (
    <div>
      Invitar amigo
      <div className="invitation-form">
        <div className="invitation-container">
          <h1>Recuperar contraseña</h1>
          <h3>Introduce el mail del amigo al que quieres invitar al grupo.</h3>
          <form onSubmit={sendEmailInvitation}>
            <label className="field-container">
              Email*
              <input
                className="field"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                type="email"
              />
            </label>

            <div className="sendmail-button-container">
              <input className="sendmail-button" type="submit" value="Enviar" />
            </div>
            {/* {error && <div className="error-label">{error} </div>} */}
          </form>
        </div>
      </div>
    </div>
  ) : (
    <div>{message}</div>
  );
}

export default InviteGroup;
