import { useState } from "react";
import "./register.css";
import logo from "../../static/Wmarl_Logo.png";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatedPassword, setRepeatedPassword] = useState("");
  const [username, setUsername] = useState("");
  const [error, setError] = useState("");
  const [checkEmail, setCheckEmail] = useState("");

  function OnSubmitRegister(event) {
    event.preventDefault();

    const error = validateRegisterForm(
      email,
      password,
      repeatedPassword,
      username
    );

    if (error) {
      setError(error);
      return;
    }

    async function performRegister() {
      const response = await fetch("http://localhost:4000/Usuarios", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          password,
          username,
        }),
      });

      const data = await response.json();
      console.log(data);

      if (data.message) {
        setError(data.message);
        return;
      }

      setCheckEmail(`Valida tu cuenta de correo, ${data.data}`);
    }

    performRegister();
    setError("");
  }

  return (
    <div className="register-container">
      <img className="wmark-register" src={logo} alt="logo register" />
      <h1>Registrate</h1>
      <form className="register-form" onSubmit={OnSubmitRegister}>
        <label className="field-container">
          Email*
          <input
            // required
            className="field"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            type="text"
          />
        </label>
        <label className="field-container">
          Password*
          <input
            // required
            className="field"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            type="password"
          />
        </label>
        <label className="field-container">
          Repeat Password*
          <input
            // required
            className="field"
            value={repeatedPassword}
            onChange={(event) => setRepeatedPassword(event.target.value)}
            type="password"
          />
        </label>
        <label className="field-container">
          Usuario*
          <input
            // required
            className="field"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
            type="text"
          />
        </label>
        <div className="register-button-container">
          <input className="register-button" type="submit" value="Register" />
        </div>
        <div>
          <p>
            ¿Ya tienes una cuenta?<a href="/login"> Log in</a>
          </p>
        </div>
        {error && <div className="error-label">{error} </div>}
        {checkEmail && <div>{checkEmail} </div>}
      </form>
    </div>
  );
}

export default Register;

function validateRegisterForm(email, password, repeatedPassword, username) {
  if (!email) {
    return "El campo email es obligatorio";
  }

  if (!password) {
    return "El campo password es obligatorio";
  }

  if (!repeatedPassword) {
    return "El campo password es obligatorio";
  }

  if (!username) {
    return "El campo username es obligatorio";
  }

  const isValidPassword = password === repeatedPassword;

  if (!isValidPassword) {
    return "Las contraseñas deben coincidir";
  }
}
