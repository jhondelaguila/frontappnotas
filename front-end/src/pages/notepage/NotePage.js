import { useHistory, useParams } from "react-router";
import { BsTrash } from "react-icons/bs";
// import ReactMarkdown from "react-markdown";
import { useEffect, useState } from "react/cjs/react.development";
import { UserContext } from "../../components/context/UserContext";
import { useContext } from "react";
import { ErrorContext } from "../../components/context/ErrorContext";
import axios from "axios";
import MDEditor from "@uiw/react-md-editor";
import "./notepage.css";

function NotePage() {
  const { idNota } = useParams();
  const [noteText, setNoteText] = useState("");
  const { user, removeNoteToUser } = useContext(UserContext);
  const { setError } = useContext(ErrorContext);
  const [note, setNote] = useState({});

  const token = user.token;

  let history = useHistory();

  useEffect(() => {
    async function getNote() {
      try {
        const response = await axios.get(
          `http://localhost:4000/grupos/notas/${idNota}`
        );
        const noteResponse = response.data.nota;
        setNote(noteResponse[0]);
      } catch (err) {
        setError(`Error abriendo pagina: ${err.response.data.message}`);
        console.log("Error", err.response);
      }
    }

    getNote();
  }, [idNota, setNote, setError, note]);

  async function removeNote(idNota) {
    try {
      await axios.delete(`http://localhost:4000/grupos/notas/${idNota}`, {
        headers: {
          Authorization: token,
        },
      });
      removeNoteToUser(idNota);
      history.push("/userhome");
    } catch (err) {
      setError(`Error borrando nota: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  async function editNote() {
    try {
      const response = await axios.put(
        `http://localhost:4000/grupos/notas/editar/${idNota}`,
        {
          contenido: noteText,
        },
        { headers: { Authorization: token } }
      );
      console.log(response);
    } catch (err) {
      setError(`Error editando nota: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return (
    <div>
      <div className="input-container">
        <MDEditor onChange={setNoteText} value={noteText} preview="edit" />
        {/* <input
          value={noteText}
          onChange={(event) => {
            const value = event.target.value;
            setNoteText(value);
          }}
          placeholder="Añade nueva nota"
          className="input-notes"
        /> */}
      </div>
      <div>
        <button onClick={() => editNote()} className="edit-button">
          Editar
        </button>
      </div>
      <section>{new Date(note.fecha_creacion).toLocaleString()}</section>
      <button
        onClick={() => removeNote(note.id)}
        className="remove-note-button"
      >
        <BsTrash />
      </button>
    </div>
  );
}

export default NotePage;
