import { useState, useContext } from "react";
import axios from "axios";
import { UserContext } from "../../components/context/UserContext";
import { ErrorContext } from "../../components/context/ErrorContext";

function AcceptInvitation() {
  const { setError } = useContext(ErrorContext);
  const { user, addGroupToUser } = useContext(UserContext);
  const [codigoGrupo, setCodigoGrupo] = useState("");
  const [message, setMessage] = useState(null);

  const idUsuario = user.id;

  async function acceptInvitation(event) {
    event.preventDefault();
    try {
      const response = await axios.put(
        `http://localhost:4000/grupos/aceptar-invitacion/${idUsuario}`,
        { codigoGrupo }
      );
      const acceptMessage = response.data.message;
      setMessage(acceptMessage);
      const newGroup = response.data.nuevoGrupo;
      addGroupToUser(newGroup);
    } catch (err) {
      setError(`Error aceptando invitación: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return message === null ? (
    <div>
      Ingresar Grupo
      <div className="accept-invitation-container">
        <h1>Acepta Invitación</h1>
        <form onSubmit={acceptInvitation}>
          <label className="field-container">
            Código de Invitación*
            <input
              className="field"
              value={codigoGrupo}
              onChange={(event) => setCodigoGrupo(event.target.value)}
              type="text"
            />
          </label>
          <div className="accept-invitation-button-container">
            <input
              className="accept-invitation-button"
              type="submit"
              value="Aceptar"
            />
          </div>
        </form>
      </div>
    </div>
  ) : (
    <div>{message}</div>
  );
}
export default AcceptInvitation;
