import ProfileNav from "../../components/profile-nav/ProfileNav";
import { useState, useContext } from "react";
import { UserContext } from "../../components/context/UserContext";
import axios from "axios";
import "./newprofilephoto.css";
import { ErrorContext } from "../../components/context/ErrorContext";

function NewProfilePhoto() {
  const [avatar, setFile] = useState();
  const { user, setAvatar } = useContext(UserContext);
  const { setError } = useContext(ErrorContext);

  console.log(user.avatar);

  // const token = user.data.token;
  // const idUsuario = user.data.id;

  function onSelectFile(event) {
    const file = event.target.files[0];

    setFile(file);
  }

  async function uploadFile(event) {
    event.preventDefault();

    try {
      const data = new FormData();
      data.append("avatar", avatar);
      const response = await axios.post(
        `http://localhost:4000/Usuarios/upload-avatar`,
        data,
        {
          headers: {
            Authorization: user.token,
          },
        }
      );

      const avatarURL = response.data.avatar;

      setAvatar(avatarURL);
    } catch (err) {
      setError(`Error cargando imagen:${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return (
    <div className="newprofilephoto-user">
      Nueva foto de perfil
      <ProfileNav />
      <form onSubmit={uploadFile}>
        <div>
          <label>
            Nueva imagen:
            <input type="file" onChange={onSelectFile} />
          </label>
          <div>
            <input disabled={!avatar} type="submit" />
          </div>
          {user.avatar && (
            <img
              src={`http://localhost:4000/uploads/${user.avatar}`}
              alt="imagen de usuario"
              className="avatar-image"
            />
          )}
        </div>
      </form>
    </div>
  );
}

export default NewProfilePhoto;
