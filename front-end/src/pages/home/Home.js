import { useHistory } from "react-router";
import nota from "../../static/nota.png";
import nota1 from "../../static/nota1.png";
import nota2 from "../../static/nota2.png";
import "./home.css";

function Home() {
  let history = useHistory();

  return (
    <div className="container-home">
      <div className="titulo-home">
        <h1>La forma más sencilla de tomar notas</h1>{" "}
        <button
          className="button-home"
          onClick={() => {
            history.push("/register");
          }}
        >
          Registrate ahora
        </button>
      </div>
      <div className="imagenes-home">
        <img src={nota1} alt="nota1" className="nota1-home" />
        <img src={nota} alt="nota" className="nota-home" />
        <img src={nota2} alt="nota2" className="nota2-home" />
      </div>
      <div className="registrate-button"></div>
    </div>
  );
}

export default Home;
