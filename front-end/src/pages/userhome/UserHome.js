import { useContext, useState } from "react";
import "./userhome.css";
import axios from "axios";
import { UserContext } from "../../components/context/UserContext";
import { ErrorContext } from "../../components/context/ErrorContext";
import ListNotes from "../listnotes/ListNotes";
import MDEditor from "@uiw/react-md-editor";

function UserHome() {
  const [noteText, setNoteText] = useState("");
  const [noteTitle, setNoteTitle] = useState("");
  const [noteGroup, setNoteGroup] = useState("");
  const [search, setSearch] = useState("");

  const { user, addNoteToUser } = useContext(UserContext);
  const { setError } = useContext(ErrorContext);

  const token = user.token;

  async function addNewNote(contenido, titulo, idGrupo) {
    try {
      const response = await axios.post(
        "http://localhost:4000/grupos/notas",
        {
          titulo,
          contenido,
          idGrupo,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      );

      const createdNote = response.data.data;
      addNoteToUser(createdNote);
    } catch (err) {
      setError(`Error añadiendo nota: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  const noteList = !search
    ? user.notas
    : user.notas.filter((note) => note.titulo.includes(search));

  return (
    <div className="userhome">
      <h1>¡Hola, {user.alias}!</h1>
      <div className="container-inputs">
        <div className="input-container">
          <input
            type="search"
            placeholder="¿Que nota estas buscando?"
            className="search-input"
            onChange={(event) => {
              const value = event.target.value;
              setSearch(value);
            }}
          />
          <input
            value={noteTitle}
            onChange={(event) => {
              const value = event.target.value;
              setNoteTitle(value);
            }}
            placeholder="Título nota"
            className="input-title"
          />
        </div>
        <div className="input-MDeditor">
          <MDEditor onChange={setNoteText} value={noteText} preview="edit" />
          {/* <input
          value={noteText}
          onChange={(event) => {
            const value = event.target.value;
            setNoteText(value);
          }}
          placeholder="Añade nueva nota"
          className="input-notes"
        /> */}
        </div>
        <div className="group-select">
          {
            <select
              onChange={(e) => {
                const value = e.target.value;
                setNoteGroup(value);
              }}
              className="grupos"
            >
              <option default>Selecciona el grupo</option>
              {user.grupos.map((groups) => {
                return (
                  <option key={groups.id} value={groups.id}>
                    {groups.titulo}
                  </option>
                );
              })}
            </select>
          }
          <button
            onClick={() => {
              const idGrupo = Number(noteGroup);

              function tieneGrupo(grupo) {
                if (grupo === isNaN) {
                  return null;
                } else {
                  return grupo;
                }
              }

              addNewNote(noteText, noteTitle, tieneGrupo(idGrupo));
              setNoteText("");
              setNoteTitle("");
            }}
            className="add-note-button"
          >
            Añadir
          </button>
        </div>
      </div>
      <div className="note-list-div">
        {" "}
        <ol>
          <div>
            {" "}
            <h4>notas</h4>
          </div>
          <div className="note-list">
            {noteList
              .sort((a, b) => {
                return new Date(b.fecha_creacion) - new Date(a.fecha_creacion);
              })
              .map((note) => (
                <ListNotes key={note.id} note={note} />
              ))}
          </div>
        </ol>
      </div>
    </div>
  );
}

export default UserHome;
