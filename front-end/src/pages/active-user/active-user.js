import { useEffect } from "react";
import axios from "axios";
import { useLocation, useHistory } from "react-router-dom";

function ActiveUser() {
  const query = new URLSearchParams(useLocation().search);
  let history = useHistory();

  const CodigoRegistro = query.get("CodigoRegistro");
  console.log(CodigoRegistro);

  useEffect(() => {
    console.log("Activando Usuario");

    try {
      async function performActivateUser() {
        const response = await axios.post(
          "http://localhost:4000/Usuarios/validacion",
          {
            CodigoRegistro,
          }
        );
        console.log(response);
      }
      history.push("/login");
      performActivateUser();
    } catch (error) {
      console.log("Error", error);
    }
  }, [CodigoRegistro, history]);

  return <div>Activando Usuario</div>;
}

export default ActiveUser;
