import { useContext, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { BsTrash } from "react-icons/bs";
import { UserContext } from "../../components/context/UserContext";
import { ErrorContext } from "../../components/context/ErrorContext";
import "./admingroup.css";
// import { Modal } from "../../components/areyousure-popup/AreYouSure";

function AdminGroups() {
  const [groupTitle, setGroupTitle] = useState("");
  const [message, setMessage] = useState(null);
  const { user, addGroupToUser, removeGroupFromUser, removeNotesbyGroup } =
    useContext(UserContext);
  const { setError } = useContext(ErrorContext);
  // const [showModal, setShowModal] = useState(false);

  const token = user.token;

  async function addNewGroup(titulo) {
    try {
      const response = await axios.post(
        "http://localhost:4000/grupos/nuevogrupo",
        {
          titulo,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      );
      console.log(response);
      const newGroup = response.data.data;
      addGroupToUser(newGroup);
    } catch (err) {
      setError(`Error creando grupo: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  async function removeGroup(idGrupo) {
    try {
      await axios.delete(`http://localhost:4000/grupos/${idGrupo}`, {
        headers: {
          Authorization: token,
        },
      });
      removeGroupFromUser(idGrupo);
      removeNotesbyGroup(idGrupo);
      //setUser(user.grupos.filter((group) => group.id !== idGrupo));
    } catch (err) {
      setError(`Error borrando grupo: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  async function abandonarGrupo(idGrupo) {
    try {
      const response = await axios.delete(
        `http://localhost:4000/grupos/abandonar-grupo/${idGrupo}`,
        { headers: { Authorization: token } }
      );

      removeGroupFromUser(idGrupo);

      setMessage(response.data.message);
    } catch (err) {
      setError(`Error abandonandoa grupo: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  // const openModal = () => {
  //   setShowModal((prev) => !prev);
  // };

  function groupType(group) {
    if (group.admin === 0) {
      return (
        <button onClick={() => abandonarGrupo(group.id)}>
          abandonar grupo
        </button>
      );

      // <Modal showModal={showModal} setShowModal={setShowModal}></Modal>
    } else {
      return (
        <button
          onClick={() => removeGroup(group.id)}
          className="remove-group-button"
        >
          <BsTrash />
        </button>
      );
    }
  }

  return (
    <div className="AdminGroup">
      <h1>Adiminstar Grupos</h1>
      <div className="input-title-container">
        <input
          value={groupTitle}
          onChange={(event) => {
            const value = event.target.value;
            setGroupTitle(value);
          }}
          placeholder="Crea un grupo nuevo"
          className="input-groups"
        />
      </div>
      <div className="group-select">
        <button
          onClick={() => {
            addNewGroup(groupTitle);
            setGroupTitle("");
          }}
          className="create-grupo-button"
        >
          Crear
        </button>
      </div>
      <ol className="group-list">
        {user.grupos
          .sort((a, b) => {
            return new Date(b.fecha_creacion) - new Date(a.fecha_creacion);
          })
          .map((group) => {
            return (
              <li key={group.id} className="group-item-container">
                {group.titulo} -{" "}
                {new Date(group.fecha_creacion).toLocaleString()}
                <div>
                  <Link to={`/invite-group/${group.id}`}>Invitar amigo</Link>
                </div>
                {groupType(group)}
              </li>
            );
          })}
      </ol>
      {message && (
        <div>
          {message}
          <button
            onClick={() => {
              setMessage(null);
            }}
          >
            ok
          </button>
        </div>
      )}
      <Link to="/acept-invitation">Ingresar grupo</Link>
    </div>
  );
}

export default AdminGroups;
