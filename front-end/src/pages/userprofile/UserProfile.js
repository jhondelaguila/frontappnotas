import "./userprofile.css";
import { useContext } from "react";
import { UserContext } from "../../components/context/UserContext";
import ProfileNav from "../../components/profile-nav/ProfileNav";

function UserProfile() {
  const { user } = useContext(UserContext);

  return (
    <div className="profile-container">
      <ProfileNav />
      <div className="profile-info">
        <img
          src={`http://localhost:4000/uploads/${user.avatar}`}
          alt="imagen de usuario"
          className="avatar-profile"
        />
        <h1>Imformación del perfil</h1>

        <h2>Alias: {user.alias}</h2>

        <h2>Email: {user.email}</h2>
      </div>
    </div>
  );
}

export default UserProfile;
