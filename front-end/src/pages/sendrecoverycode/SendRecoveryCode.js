import { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { ErrorContext } from "../../components/context/ErrorContext";
import "./sendrecoverycode.css";
import logo from "../../static/Wmarl_Logo.png";

function SendRecoveryCode() {
  const { setError } = useContext(ErrorContext);
  const [email, setEmail] = useState("");

  // const { email } = useParams();

  let history = useHistory();

  async function onSubmitSendRecoveryCode(e) {
    e.preventDefault();
    try {
      await axios.put(
        "http://localhost:4000/Usuarios/contrasena/recuperarContrasena",
        {
          email,
        }
      );

      //history push a /userhome/profile/introducerecoverycode

      history.push("/userhome/introducerecoverycode");
    } catch (err) {
      setError(
        `Error enviando Código de Recuperación: ${err.response.data.message}`
      );
      console.log("Error", err.response);
    }
  }

  return (
    <div>
      <div className="recovery-code-form">
        <img className="wmark-recoverycode" src={logo} alt="logo recorycode" />
        <h1>¿Olvidaste la contraseña?</h1>
        <div className="recovery-code-container">
          <p>
            Introduce tu email y te enviaremos un código para cambiar la
            contraseña.
          </p>
          <form className="send-email" onSubmit={onSubmitSendRecoveryCode}>
            <label className="field-container">
              Email*
              <input
                className="field"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                type="email"
              />
            </label>

            <div className="sendmail-button-container">
              <input className="sendmail-button" type="submit" value="Enviar" />
            </div>
            {/* {error && <div className="error-label">{error} </div>} */}
          </form>
        </div>
      </div>
    </div>
  );
}

export default SendRecoveryCode;
