import ProfileNav from "../../components/profile-nav/ProfileNav";
import { useState, useContext } from "react";
import { UserContext } from "../../components/context/UserContext";
import axios from "axios";
import { ErrorContext } from "../../components/context/ErrorContext";
import "./newpassword.css";
function NewPassword() {
  const { user, addNewPassword } = useContext(UserContext);
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [repeatedNewPassword, setRepeatedNewPassword] = useState("");
  const { error, setError } = useContext(ErrorContext);

  const idUsuario = user.id;
  const token = user.token;
  const userPassword = user.password;

  async function onSubmitNewPassword(event) {
    event.preventDefault();

    const error = validateRegisterForm(
      password,
      newPassword,
      repeatedNewPassword,
      userPassword
    );

    if (error) {
      setError(error);
      return;
    }

    try {
      await axios.put(
        `http://localhost:4000/Usuarios/${idUsuario}/password`,
        {
          password,
          newPassword,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      );
      addNewPassword(newPassword);
    } catch (err) {
      setError(`Error cambiando contraseña: ${err.response.data.message}`);
      console.log("Error", err.response);
    }
  }

  return user.password === newPassword ? (
    <h3>Tu contraseña ha sido cambiada correctamente{user.password}</h3>
  ) : (
    <div className="newpassword-user">
      Nueva contraseña
      <ProfileNav />
      <div className="newpassword-form">
        <div className="newpassword-container">
          <h1>Cambiar contraseña</h1>
          <form onSubmit={onSubmitNewPassword}>
            <label className="field-container">
              Contraseña actual*
              <input
                className="field"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                type="password"
              />
            </label>
            <label className="field-container">
              New Password*
              <input
                // required
                className="field"
                value={newPassword}
                onChange={(event) => setNewPassword(event.target.value)}
                type="password"
              />
            </label>
            <label className="field-container">
              Repeat New Password*
              <input
                // required
                className="field"
                value={repeatedNewPassword}
                onChange={(event) => setRepeatedNewPassword(event.target.value)}
                type="password"
              />
            </label>
            <div className="newpassword-button-container">
              <input
                className="newpassword-button"
                type="submit"
                value="Cambiar contraseña"
              />
            </div>
            {error && <div className="error-label">{error} </div>}
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewPassword;

function validateRegisterForm(
  password,
  newPassword,
  repeatedNewPassword,
  userPassword
) {
  if (!password) {
    return "El campo password es obligatorio";
  }

  if (!newPassword) {
    return "El campo password es obligatorio";
  }

  if (!repeatedNewPassword) {
    return "El campo password es obligatorio";
  }
  const correctPassword = password === userPassword;

  if (!correctPassword) {
    return "La contraseña actual es incorrecta";
  }

  const isValidPassword = newPassword === repeatedNewPassword;

  if (!isValidPassword) {
    return "Las contraseñas deben coincidir";
  }
}
