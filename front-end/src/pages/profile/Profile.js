import "./profile.css";

function Profile() {
  return (
    <aside className="nav-profile">
      <nav className="nav-aside">
        <ul>
          <li>
            <a href="/userhome/profile/info">Información Usuario</a>
          </li>
          <li>
            <a href="/userhome/profile/admingroups">Administrar Grupos</a>
          </li>
        </ul>
      </nav>
    </aside>
  );
}

export default Profile;
