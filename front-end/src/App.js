import { Route, Switch, useHistory } from "react-router-dom";
import "./App.css";
import { useContext } from "react";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import About from "./pages/about/About";
import Register from "./pages/register/register";
import ActiveUser from "./pages/active-user/active-user";
import HeaderActivated from "./components/headers/headeractived/HeaderActivated";
import UserHome from "./pages/userhome/UserHome";
import { UserContext } from "./components/context/UserContext";
import UserProfile from "./pages/userprofile/UserProfile";
import SendRecoveryCode from "./pages/sendrecoverycode/SendRecoveryCode";
import IntroduceRecoveryCode from "./pages/introducerecoverycode/IntroduceRecoveryCode";
import NewProfilePhoto from "./pages/newprofilephoto/NewProfilePhoto";
import AdminGroups from "./pages/admingroups/AdminGroups";
import Contact from "./pages/contact/Contact";
import { ErrorBanner } from "./components/errorbanner/ErrorBanner";
import InviteGroup from "./pages/invitegroup/InviteGroup";
import AcceptInvitation from "./pages/acceptinvitation/AcceptInvitation";
import NewPassword from "./pages/newpassword/NewPassword";
import NotePage from "./pages/notepage/NotePage";
import Profile from "./pages/profile/Profile";
import Footer from "./components/footer/Footer";

function PrivateRoute({ children }) {
  const { user } = useContext(UserContext);
  const history = useHistory();

  if (!user) {
    history.push("/login");
    return null;
  }

  return children;
}

function App() {
  return (
    <div className="App">
      <ErrorBanner />
      <HeaderActivated />
      <Switch>
        <Route path="/acept-invitation" exact>
          <PrivateRoute>
            <AcceptInvitation />
          </PrivateRoute>
        </Route>
        <Route path="/invite-group/:idGrupo" exact>
          <PrivateRoute>
            <InviteGroup />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/profile/admingroups" exact>
          <PrivateRoute>
            <AdminGroups />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/introducerecoverycode" exact>
          <IntroduceRecoveryCode />
        </Route>
        <Route path="/userhome/sendrecoverycode" exact>
          <SendRecoveryCode />
        </Route>
        <Route path="/userhome/profile/newpassword" exact>
          <PrivateRoute>
            <NewPassword />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/profile/newprofilephoto" exact>
          <PrivateRoute>
            <NewProfilePhoto />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/profile/info" exact>
          <PrivateRoute>
            <UserProfile />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/profile" exact>
          <PrivateRoute>
            <Profile />
          </PrivateRoute>
        </Route>
        <Route path="/userhome/notes/:idNota" exact>
          <PrivateRoute>
            <NotePage />
          </PrivateRoute>
        </Route>
        <Route path="/userhome" exact>
          <PrivateRoute>
            <UserHome />
          </PrivateRoute>
        </Route>
        <Route path="/login" exact>
          <Login />
        </Route>
        <Route path="/about" exact>
          <About />
        </Route>
        <Route path="/contact" exact>
          <Contact />
        </Route>
        <Route path="/register" exact>
          <Register />
        </Route>
        <Route path="/activate-user" exact>
          <ActiveUser />
        </Route>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="*">
          <div>Not found</div>
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;

// function getTokenFromStorage(){
//   const accessToken = sessionStorage.getItem("accesToken");

//   return accessToken;
// }
