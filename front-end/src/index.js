import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import { ErrorContextProvider } from "./components/context/ErrorContext";
import { UserContextProvider } from "./components/context/UserContext";

ReactDOM.render(
  <React.StrictMode>
    <ErrorContextProvider>
      <UserContextProvider>
        <Router>
          <App />
        </Router>
      </UserContextProvider>
    </ErrorContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
