import { useContext } from "react";
import { ErrorContext } from "../context/ErrorContext";

export const ErrorBanner = () => {
  const { error, setError } = useContext(ErrorContext);

  return error ? (
    <div className="error">
      <p>{error}</p>
      <button onClick={() => setError(null)}>Ok!</button>
    </div>
  ) : null;
};
