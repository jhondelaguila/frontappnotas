import { createContext, useState } from "react";

export const UserContext = createContext();

export const UserContextProvider = ({ children }) => {
  const localStorageUser = localStorage.getItem("user");
  const [user, setUser] = useState(
    localStorageUser ? JSON.parse(localStorageUser) : null
  );

  function login(data) {
    localStorage.setItem("user", JSON.stringify(data));
    setUser(data);
  }

  function logout() {
    localStorage.removeItem("user");

    setUser(null);
  }

  function addNewPassword(newpassword) {
    setUser((user) => {
      const newUser = {
        ...user,
        password: newpassword,
      };

      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function addNoteToUser(note) {
    setUser((user) => {
      const newUser = {
        ...user,
        notas: [...user.notas, note],
      };

      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function removeNoteToUser(note) {
    setUser((user) => {
      const newUser = {
        ...user,
        notas: user.notas.filter((notes) => notes.id !== note),
      };
      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function addGroupToUser(group) {
    setUser((user) => {
      const newUser = {
        ...user,
        grupos: [...user.grupos, group],
      };

      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function removeNotesbyGroup(idgroup) {
    setUser((user) => {
      const newUser = {
        ...user,
        notas: user.notas.filter((notes) => notes.id_grupo !== idgroup),
      };
      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function removeGroupFromUser(group) {
    setUser((user) => {
      const newUser = {
        ...user,
        grupos: user.grupos.filter((grupo) => grupo.id !== group),
      };

      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  function setAvatar(avatar) {
    setUser((user) => {
      const newUser = {
        ...user,
        avatar,
      };

      localStorage.setItem("user", JSON.stringify(newUser));

      return newUser;
    });
  }

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        login,
        logout,
        setAvatar,
        addGroupToUser,
        removeGroupFromUser,
        removeNoteToUser,
        addNoteToUser,
        addNewPassword,
        removeNotesbyGroup,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
