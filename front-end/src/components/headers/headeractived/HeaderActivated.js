import HeaderUser from "../headeruser/HeaderUser";
import HeaderDefault from "../headerdefault/HeaderDefault";
import { useContext } from "react";
import { UserContext } from "../../context/UserContext";

function HeaderActivated() {
  const { user } = useContext(UserContext);

  return <header>{user ? <HeaderUser /> : <HeaderDefault />}</header>;
}

export default HeaderActivated;
