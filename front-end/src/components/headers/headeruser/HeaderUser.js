import { useContext } from "react";
import { UserContext } from "../../context/UserContext";
import "./headeruser.css";
import logo from "../../../static/Wmarl_Logo.png";
import defaultphoto from "../../../static/defaultphoto.png";
import { HiMenu } from "react-icons/hi";

function HeaderUser() {
  const { logout, user } = useContext(UserContext);
  const logo1 = <img src={logo} alt="logo web" className="logo-image" />;

  return (
    <div className="container-header">
      <div className="contenedor">
        <a href="/userhome" className="logo-web">
          {logo1}
        </a>
        <input type="checkbox" id="menu-bar-user" />
        <label htmlFor="menu-bar-user">
          <HiMenu className="himenu-user" />
        </label>
        <nav className="navigation-user">
          <ul className="nav">
            <li className="list-hover">
              <a href="/userhome">Inicio</a>
            </li>
            <li className="list-hover">
              <a href="/contact">Contacto</a>
            </li>
            <li className="list-hover">
              <a href="/about">Acerca de</a>
            </li>
            <li className="image-user-list">
              <a href="/userhome/profile" className="a-image">
                {!user.avatar ? (
                  <img
                    src={defaultphoto}
                    alt="imagen de usuario"
                    className="user-image"
                  />
                ) : (
                  <img
                    src={`http://localhost:4000/uploads/${user.avatar}`}
                    alt="imagen de usuario"
                    className="user-image"
                  />
                )}
              </a>
              <ul>
                <li className="list-hover">
                  <a href="/userhome/profile/info">Información Usuario</a>
                </li>
                <li className="list-hover">
                  <a href="/userhome/profile/admingroups">Administrar Grupos</a>
                </li>
                <li className="list-hover" onClick={() => logout()}>
                  <a href="/login">Cerrar sesión</a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}

export default HeaderUser;
