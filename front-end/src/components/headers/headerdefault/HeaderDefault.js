import "./headerdefault.css";
import logo from "../../../static/Wmarl_Logo.png";
import { HiMenu } from "react-icons/hi";

function HeaderDefault() {
  const logo1 = <img src={logo} alt="logo web" className="logo-image" />;
  return (
    <div className="App-header">
      <div className="header-default-container">
        <a href="/" className="img-logo">
          {logo1}
        </a>
        <input type="checkbox" id="menu-bar" />
        <label htmlFor="menu-bar">
          <HiMenu className="himenu" />
        </label>
        <nav className="menu">
          <a href="/">Home</a>
          <a href="/login">Login</a>
          <a href="/about">About</a>
          <a href="/register ">Registrate</a>
        </nav>
      </div>
    </div>
  );
}

export default HeaderDefault;
