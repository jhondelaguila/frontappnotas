import "./footer.css";

function Footer() {
  return (
    <footer className="container-footer">
      <div className="social-links">
        <ul>
          <li>
            <a href="https://www.facebook.com/">
              <img
                src="https://evernote.com/c/assets/social/facebook.svg?e18f2abead894dbc/"
                alt="facebook footer"
              />
            </a>
          </li>
          <li>
            <a href="https://twitter.com">
              <img
                src="https://evernote.com/c/assets/social/twitter.svg?6e14e1ba47c21524"
                alt="twitter footer"
              />
            </a>
          </li>
        </ul>
      </div>
      <div className="row-legal">
        <p className="copyright">
          © 2021 WMark Corporation. Todos los derechos reservados.
        </p>
        <ul>
          <li>Seguridad</li>
          <li>Legal</li>
          <li>Pivacidad</li>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
