import "./profilenav.css";

function ProfileNav() {
  return (
    <aside>
      <nav className="nav-aside">
        <ul className="list">
          <li>
            <a href="/userhome/profile/newpassword">Cambiar contraseña</a>
          </li>
          <li>
            <a href="/userhome/profile/newprofilephoto">
              Cambiar foto de perfil
            </a>
          </li>
        </ul>
      </nav>
    </aside>
  );
}

export default ProfileNav;
